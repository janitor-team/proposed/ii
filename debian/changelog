ii (1.8-3+ds) unstable; urgency=medium

  * Fix lintian: package-uses-old-debhelper-compat-version
  * autopkgtest smoke-test is superficial
  * Update Vcs-Git/Browser URL
  * Bump standards version to 4.6.0 (no changes required)
  * Update e-mail address
  * Bump debian/watch version to 4
  * Add Bug-Submit metadata
  * Add Upstream-Contact to d/copyright

 -- itd <itd@net.in.tum.de>  Sat, 13 Nov 2021 22:57:12 +0100

ii (1.8-2) unstable; urgency=medium

  * PATH_MAX may be undefined (e.g. on Hurd)
  * Bump standards version to 4.2.1 (no changes required).

 -- itd <itd@firemail.cc>  Mon, 03 Dec 2018 21:34:53 +0000

ii (1.8-1) unstable; urgency=medium

  * New upstream release (Closes: #890995).
  * Set myself as Maintainer. (Nico Golde has retired. Thanks for your work!)
  * Relicense patches to match upstream's license. (See: #902225)
  * Fix watchfile (upstream URL changed).
  * Bump compat level to 11 and debhelper dependency (>= 11.3.2~).
  * Bump standards version to 4.2.0.
  * Update patch respect_dpkg_buildflags.patch.
  * Update hardening option (requires dpkg-dev >= 1.16.1.1).
  * Switch to machine-readable copyright file.
  * Trivial autopkgtest smoke-test.
  * Switch to HTTPS upstream links.
  * Switch to "dh7" style rules file.
  * Set 'Rules-Requires-Root: no'.
  * Use libbsd-dev's strlcpy(3).

 -- itd <itd@firemail.cc>  Thu, 09 Aug 2018 14:28:03 +0200

ii (1.7-2) unstable; urgency=high

  * Enable hardening options.
  * Introduce build-arch and build-indep targets to rules.
  * Fix upstream homepage (Closes: #693116).
  * Do not explicitly include /usr/lib as library search path,
    but just use the default (Closes: #722784).

 -- Nico Golde <nion@debian.org>  Sun, 29 Sep 2013 17:22:44 +0200

ii (1.7-1) unstable; urgency=low

  * New upstream release.
  * Bump standards version (no changes required).
  * debian/control: fix upstream homepage.

 -- Nico Golde <nion@debian.org>  Sat, 05 Jan 2013 17:50:18 +0100

ii (1.6-1) unstable; urgency=low

  * New upstream release.

 -- Nico Golde <nion@debian.org>  Mon, 31 Jan 2011 21:48:11 +0100

ii (1.5-1) unstable; urgency=low

  * New upstream release.
  * Switch to dpkg-source 3.0 (quilt) format
  * Bump policy version, no changes needed
  * Use paragraphs rather than asterisks in NEWS.Debian

 -- Nico Golde <nion@debian.org>  Wed, 26 Jan 2011 12:15:37 +0100

ii (1.4-3) unstable; urgency=low

  * Replace echo with printf in query.sh so it doesn't need
    to rely on the XSI extension of SuSv3 (Closes: #530976).

 -- Nico Golde <nion@debian.org>  Mon, 01 Jun 2009 13:40:33 +0200

ii (1.4-2) unstable; urgency=low

  * Remove unneeded bashism in query script (Closes: #530103).
  * Small copyright adjustments, nothing worth to mention.
  * Bump policy version, no changes needed.
  * Use dh_prep rather than dh_clean -k in debian/rules.

 -- Nico Golde <nion@debian.org>  Sat, 23 May 2009 17:19:37 +0200

ii (1.4-1) unstable; urgency=low

  * New upstream release.

 -- Nico Golde <nion@debian.org>  Sat, 09 Aug 2008 13:48:56 +0200

ii (1.3-2) unstable; urgency=low

  * Bump to policy version 3.8.0, no changes needed.
  * Bump compat level and debhelper dependency to 7.
  * Make Homepage a real control field and fix URL.
  * Fix tarball localtion in watch file.

 -- Nico Golde <nion@debian.org>  Tue, 22 Jul 2008 18:54:24 +0200

ii (1.3-1) unstable; urgency=low

  * New upstream release.

 -- Nico Golde <nion@debian.org>  Sat, 14 Jul 2007 20:49:01 +0200

ii (1.2-1) unstable; urgency=low

  * New upstream release.

 -- Nico Golde <nion@debian.org>  Sat, 23 Jun 2007 13:45:51 +0200

ii (1.1-3) unstable; urgency=low

  * FIxed copyright years.

 -- Nico Golde <nion@debian.org>  Sat, 09 Jun 2007 14:39:59 +0200

ii (1.1-2) unstable; urgency=low

  * Handing package over to Nico.

 -- Daniel Baumann <daniel@debian.org>  Mon, 23 Apr 2007 08:42:00 +0200

ii (1.1-1) unstable; urgency=low

  * New upstream release.

 -- Nico Golde <nion@debian.org>  Wed, 04 Apr 2007 10:41:46 +0200

ii (1+fixed-3) unstable; urgency=low

  * Updated Nicos email address.

 -- Daniel Baumann <daniel@debian.org>  Wed, 28 Mar 2007 21:41:00 +0100

ii (1+fixed-2) unstable; urgency=low

  * Updated homepage in control and copyright.

 -- Daniel Baumann <daniel@debian.org>  Wed, 14 Mar 2007 09:45:00 +0100

ii (1+fixed-1) unstable; urgency=low

  * Upstream did change the release tarball again.

 -- Daniel Baumann <daniel@debian.org>  Sat,  3 Feb 2007 21:50:00 +0100

ii (1-1) unstable; urgency=low

  * Initial release (Closes: #340682).

 -- Daniel Baumann <daniel@debian.org>  Sat,  3 Feb 2007 19:08:00 +0100
